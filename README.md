# 3D Renderer

## Prerequisites

You will need python 3, numpy and pygame installed. I recommend creating a venv to install these things.

## How To Use

The scripts from 03 on can load .obj files. Use the method `load_mesh_from_object_file` for that. There are already two
.obj files checked in you can use. It might not be compatible with all .obj files since it is a very rudimentary parser.

Please be aware that script from 03 on are refactored a bit to work with numpy arrays instead of the Vec3d class
initially introduced.