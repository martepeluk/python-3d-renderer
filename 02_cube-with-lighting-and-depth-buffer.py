# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import pygame
import numpy as np

class Vec3d:
    def __init__(self, x: float, y: float, z: float):
        self.x = x
        self.y = y
        self.z = z

    def to_numpy_array(self):
        return np.array([self.x, self.y, self.z])

class Triangle:
    def __init__(self, points: list[Vec3d], color=pygame.Color("green")):
        if len(points) != 3:
            raise Exception('points has wrong length of ' + len(points).__str__())
        self.points = points
        self.color = color

class Mesh:
    def __init__(self, triangles: list[Triangle]):
        self.triangles = triangles


def create_cube() -> Mesh:
    south1 = Triangle([Vec3d(0, 0, 0), Vec3d(0, 1, 0), Vec3d(1, 1, 0)])
    south2 = Triangle([Vec3d(0, 0, 0), Vec3d(1, 1, 0), Vec3d(1, 0, 0)])

    east1 = Triangle([Vec3d(1, 0, 0), Vec3d(1, 1, 0), Vec3d(1, 1, 1)])
    east2 = Triangle([Vec3d(1, 0, 0), Vec3d(1, 1, 1), Vec3d(1, 0, 1)])

    north1 = Triangle([Vec3d(1, 0, 1), Vec3d(1, 1, 1), Vec3d(0, 1, 1)])
    north2 = Triangle([Vec3d(1, 0, 1), Vec3d(0, 1, 1), Vec3d(0, 0, 1)])

    west1 = Triangle([Vec3d(0, 0, 1), Vec3d(0, 1, 1), Vec3d(0, 1, 0)])
    west2 = Triangle([Vec3d(0, 0, 1), Vec3d(0, 1, 0), Vec3d(0, 0, 0)])

    top1 = Triangle([Vec3d(0, 1, 0), Vec3d(0, 1, 1), Vec3d(1, 1, 1)])
    top2 = Triangle([Vec3d(0, 1, 0), Vec3d(1, 1, 1), Vec3d(1, 1, 0)])

    bottom1 = Triangle([Vec3d(1, 0, 1), Vec3d(0, 0, 1), Vec3d(0, 0, 0)])
    bottom2 = Triangle([Vec3d(1, 0, 1), Vec3d(0, 0, 0), Vec3d(1, 0, 0)])

    return Mesh([south1, south2, east1, east2, north1, north2, west1, west2, top1, top2, bottom1, bottom2])

SCREEN_HEIGHT = 640
SCREEN_WIDTH = 480
F_NEAR = 0.1
F_FAR = 1000.0
F_FOV = 90.0
F_ASPECT_RATIO = SCREEN_HEIGHT / SCREEN_WIDTH
F_FOV_RAD = 1.0 / np.tan(F_FOV * 0.5 / 180.0 * np.pi)
PROJECTION_MATRIX = np.array([[F_ASPECT_RATIO * F_FOV_RAD, 0, 0, 0],
                              [0, F_FOV_RAD, 0, 0],
                              [0, 0, F_FAR / (F_FAR - F_NEAR), 1.0],
                              [0, 0, (-F_FAR * F_NEAR) / (F_FAR - F_NEAR), 0]])

CAMERA_POSITION = Vec3d(0, 0, 0)


def x_rotation_matrix(theta):
    return np.array([[1.0, 0, 0, 0],
                     [0, np.cos(theta), np.sin(theta), 0],
                     [0, -np.sin(theta), np.cos(theta), 0],
                     [0, 0, 0, 1.0]])


def z_rotation_matrix(theta):
    return np.array([[np.cos(theta), np.sin(theta), 0, 0],
                     [-np.sin(theta), np.cos(theta), 0, 0],
                     [0, 0, 1.0, 0],
                     [0, 0, 0, 1.0]])


def project_point(point, matrix) -> Vec3d:
    array_point = np.array([point.x, point.y, point.z, 1])
    projected_point = np.matmul(array_point, matrix)
    if projected_point[3] != 0:
        projected_point = projected_point / projected_point[3]
    return Vec3d(projected_point[0], projected_point[1], projected_point[2])


def copy_triangle(to_be_copied: Triangle) -> Triangle:
    return Triangle([Vec3d(to_be_copied.points[0].x, to_be_copied.points[0].y, to_be_copied.points[0].z),
                     Vec3d(to_be_copied.points[1].x, to_be_copied.points[1].y, to_be_copied.points[1].z),
                     Vec3d(to_be_copied.points[2].x, to_be_copied.points[2].y, to_be_copied.points[2].z)])


def normalized_array(array):
    norm = np.sqrt(array[0] * array[0] + array[1] * array[1] + array[2] * array[2])
    if norm == 0:
        return array
    return array / norm

def main():
    pygame.init()

    pygame.display.set_caption("3d Rendere Engine")

    window = pygame.display.set_mode((SCREEN_HEIGHT, SCREEN_WIDTH))

    running = True

    cubeMesh = create_cube()

    old_time = pygame.time.get_ticks()

    rot_theta = 0

    # radius = 1
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        new_time = pygame.time.get_ticks()
        elapsed_time = new_time - old_time
        rot_theta += 0.001 * elapsed_time

        window.fill(pygame.Color("black"))
        for tri in cubeMesh.triangles:

            # rotate in y-axis
            triz1 = project_point(tri.points[0], z_rotation_matrix(rot_theta))
            triz2 = project_point(tri.points[1], z_rotation_matrix(rot_theta))
            triz3 = project_point(tri.points[2], z_rotation_matrix(rot_theta))

            # rotate in x-axis
            trizx1 = project_point(triz1, x_rotation_matrix(rot_theta))
            trizx2 = project_point(triz2, x_rotation_matrix(rot_theta))
            trizx3 = project_point(triz3, x_rotation_matrix(rot_theta))

            # offset into the screen
            z_offset = 5.0
            translated_triangle = Triangle([trizx1, trizx2, trizx3])
            translated_triangle.points[0].z += z_offset
            translated_triangle.points[1].z += z_offset
            translated_triangle.points[2].z += z_offset

            line1 = Vec3d(translated_triangle.points[1].x - translated_triangle.points[0].x,
                          translated_triangle.points[1].y - translated_triangle.points[0].y,
                          translated_triangle.points[1].z - translated_triangle.points[0].z
                          )

            line2 = Vec3d(translated_triangle.points[2].x - translated_triangle.points[0].x,
                          translated_triangle.points[2].y - translated_triangle.points[0].y,
                          translated_triangle.points[2].z - translated_triangle.points[0].z
                          )

            normal = normalized_array(np.cross(line1.to_numpy_array(), line2.to_numpy_array()))

            # if normal[2] > 0:
            # if True:
            # Dot product for z buffering
            if normal[0] * (translated_triangle.points[0].x - CAMERA_POSITION.x) + \
                    normal[1] * (translated_triangle.points[0].y - CAMERA_POSITION.y) + \
                    normal[2] * (translated_triangle.points[0].z - CAMERA_POSITION.z) < 0:

                # Light from top right
                # light_direction = normalized_array(np.array([1.0, -1.0, -1.0]))

                # light from front
                light_direction = normalized_array(np.array([0.0, 0.0, -1.0]))
                dot_product = np.dot(normal, light_direction)

                # Project triangles from 3D --> 2D
                tri1 = project_point(translated_triangle.points[0], PROJECTION_MATRIX)
                tri2 = project_point(translated_triangle.points[1], PROJECTION_MATRIX)
                tri3 = project_point(translated_triangle.points[2], PROJECTION_MATRIX)

                # scale into view
                tri1.x += 1.0
                tri1.y += 1.0
                tri2.x += 1.0
                tri2.y += 1.0
                tri3.x += 1.0
                tri3.y += 1.0

                tri1.x *= 0.5 * SCREEN_WIDTH
                tri1.y *= 0.5 * SCREEN_HEIGHT
                tri2.x *= 0.5 * SCREEN_WIDTH
                tri2.y *= 0.5 * SCREEN_HEIGHT
                tri3.x *= 0.5 * SCREEN_WIDTH
                tri3.y *= 0.5 * SCREEN_HEIGHT

                projected_color = pygame.Color("green")
                luminance = np.floor((dot_product + 1) * 40)
                projected_color.hsla = (207, 100, luminance, 1)
                # projected_color.hsla = (207, 100, 80, 1)

                # pygame.draw.polygon(surface=window, color=pygame.Color("black"), points=[(tri1.x, tri1.y), (tri2.x, tri2.y), (tri3.x, tri3.y)], width=5)
                pygame.draw.polygon(surface=window, color=projected_color, points=[(tri1.x, tri1.y), (tri2.x, tri2.y), (tri3.x, tri3.y)], width=0)

        pygame.display.update()
        old_time = new_time


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
