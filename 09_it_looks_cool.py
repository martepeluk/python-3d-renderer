# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import pygame
import numpy as np

# class Vec3d:
#     def __init__(self, x: float, y: float, z: float):
#         self.x = x
#         self.y = y
#         self.z = z
#
#     def to_numpy_array(self):
#         return np.array([self.x, self.y, self.z])

class Triangle:
    def __init__(self, points: list[np.ndarray], color=pygame.Color("green")):
        if len(points) != 3:
            raise Exception('points has wrong length of ' + len(points).__str__())
        self.points = points
        self.color = color

class Mesh:
    def __init__(self, triangles: list[Triangle]):
        self.triangles = triangles

SCREEN_HEIGHT = 640
SCREEN_WIDTH = 480
F_NEAR = 0.1
F_FAR = 1000.0
F_FOV = 90.0
F_ASPECT_RATIO = SCREEN_HEIGHT / SCREEN_WIDTH
F_FOV_RAD = 1.0 / np.tan(F_FOV * 0.5 / 180.0 * np.pi)
PROJECTION_MATRIX = np.array([[F_ASPECT_RATIO * F_FOV_RAD, 0, 0, 0],
                              [0, F_FOV_RAD, 0, 0],
                              [0, 0, F_FAR / (F_FAR - F_NEAR), 1.0],
                              [0, 0, (-F_FAR * F_NEAR) / (F_FAR - F_NEAR), 0]])

CAMERA_POSITION = np.array([0, 0, 0])


def x_rotation_matrix(theta):
    return np.array([[1.0, 0, 0, 0],
                     [0, np.cos(theta), np.sin(theta), 0],
                     [0, -np.sin(theta), np.cos(theta), 0],
                     [0, 0, 0, 1.0]])


def z_rotation_matrix(theta):
    return np.array([[np.cos(theta), np.sin(theta), 0, 0],
                     [-np.sin(theta), np.cos(theta), 0, 0],
                     [0, 0, 1.0, 0],
                     [0, 0, 0, 1.0]])


def project_point(point, matrix) -> np.ndarray:
    array_point = np.array([point[0], point[1], point[2], 1])
    projected_point = np.matmul(array_point, matrix)
    if projected_point[3] != 0:
        projected_point = projected_point / projected_point[3]
    return np.array([projected_point[0], projected_point[1], projected_point[2]])


def copy_triangle(to_be_copied: Triangle) -> Triangle:
    return Triangle([np.array([to_be_copied.points[0][0], to_be_copied.points[0][1], to_be_copied.points[0][2]]),
                     np.array(to_be_copied.points[1][0], to_be_copied.points[1][1], to_be_copied.points[1][2]),
                     np.array(to_be_copied.points[2][0], to_be_copied.points[2][1], to_be_copied.points[2][2])])


def normalized_array(array):
    norm = np.sqrt(array[0] * array[0] + array[1] * array[1] + array[2] * array[2])
    if norm == 0:
        return array
    return array / norm


def load_mesh_from_object_file(filename: str) -> Mesh:
    file = open(filename, 'r')
    lines = file.readlines()

    triangles = []
    vertices = []

    for line in lines:
        line_array = line.strip().split()
        if not line_array:
            continue
        if line_array[0] == 'v' or line_array[0] == 'vn':
            vertex = np.array([float(line_array[1]), float(line_array[2]), float(line_array[3])])
            vertices.append(vertex)
        if line_array[0] == 'f':
            triangle = Triangle([vertices[int(line_array[1]) - 1], vertices[int(line_array[2]) - 1], vertices[int(line_array[3]) - 1]])
            # triangle = Triangle([vertices[int(line_array[1].split("/")[0]) - 1], vertices[int(line_array[2].split("/")[0]) - 1], vertices[int(line_array[3].split("/")[0]) - 1]])
            triangles.append(triangle)
    return Mesh(triangles)


def average_z(triangle: Triangle):
    return - (triangle.points[0][2] + triangle.points[1][2] + triangle.points[2][2]) / 3.0


def main():
    pygame.init()

    pygame.display.set_caption("3d Renderer Engine")

    window = pygame.display.set_mode((SCREEN_HEIGHT, SCREEN_WIDTH))

    running = True

    cubeMesh = load_mesh_from_object_file("teapot.obj")

    old_time = pygame.time.get_ticks()

    rot_theta = 0.5

    # radius = 1
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        new_time = pygame.time.get_ticks()
        elapsed_time = new_time - old_time
        rot_theta += 0.001 * elapsed_time

        # window.fill(pygame.Color("black"))
        # radius = 100
        # pygame.draw.circle(window, (0, 0, 255), (640, 480), radius, 0)

        triangles_to_raster = []
        for tri in cubeMesh.triangles:

            # rotate in y-axis
            z_rot_matrix = x_rotation_matrix(rot_theta)
            triz1 = project_point(tri.points[0], z_rot_matrix)
            triz2 = project_point(tri.points[1], z_rot_matrix)
            triz3 = project_point(tri.points[2], z_rot_matrix)

            # rotate in x-axis
            # x_rot_matrix = x_rotation_matrix(rot_theta)
            # trizx1 = project_point(triz1, x_rot_matrix)
            # trizx2 = project_point(triz2, x_rot_matrix)
            # trizx3 = project_point(triz3, x_rot_matrix)

            # offset into the screen
            z_offset = 10.0
            translated_triangle = Triangle([triz1, triz2, triz3])
            translated_triangle.points[0][2] += z_offset
            translated_triangle.points[1][2] += z_offset
            translated_triangle.points[2][2] += z_offset

            line1 = translated_triangle.points[1] - translated_triangle.points[0]
            line2 = translated_triangle.points[2] - translated_triangle.points[0]

            normal = normalized_array(np.cross(line1, line2))

            # if True:
            if np.dot(normal, translated_triangle.points[0] - CAMERA_POSITION) < 0:

                light_direction = normalized_array(np.array([0.0, 0.0, -1.0]))
                dot_product = np.dot(normal, light_direction)

                # Project triangles from 3D --> 2D
                tri1 = project_point(translated_triangle.points[0], PROJECTION_MATRIX)
                tri2 = project_point(translated_triangle.points[1], PROJECTION_MATRIX)
                tri3 = project_point(translated_triangle.points[2], PROJECTION_MATRIX)

                # scale into view
                tri1[0] += 1.0
                tri1[1] += 1.0
                tri2[0] += 1.0
                tri2[1] += 1.0
                tri3[0] += 1.0
                tri3[1] += 1.0

                tri1[0] *= 0.5 * SCREEN_WIDTH
                tri1[1] *= 0.5 * SCREEN_HEIGHT
                tri2[0] *= 0.5 * SCREEN_WIDTH
                tri2[1] *= 0.5 * SCREEN_HEIGHT
                tri3[0] *= 0.5 * SCREEN_WIDTH
                tri3[1] *= 0.5 * SCREEN_HEIGHT

                projected_color = pygame.Color("green")
                luminance = np.floor((dot_product + 1) * 50)
                projected_color.hsla = (207, 100, luminance, 1)

                projected_triangle = Triangle([tri1, tri2, tri3], projected_color)
                triangles_to_raster.append(projected_triangle)

        # Comment out to not sort the depth
        triangles_to_raster.sort(key=average_z)

        for tri_projected in triangles_to_raster:
            draw_color = tri_projected.color
            # Show without light
            # draw_color.hsla = (207, 100, 50, 1)
            # pygame.draw.polygon(surface=window, color=pygame.Color("black"), points=[(tri_projected.points[0][0], tri_projected.points[0][1]), (tri_projected.points[1][0], tri_projected.points[1][1]), (tri_projected.points[2][0], tri_projected.points[2][1])], width=4)
            pygame.draw.polygon(surface=window, color=draw_color, points=[(tri_projected.points[0][0], tri_projected.points[0][1]), (tri_projected.points[1][0], tri_projected.points[1][1]), (tri_projected.points[2][0], tri_projected.points[2][1])], width=0)

        pygame.display.update()
        old_time = new_time


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
