# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import pygame
import numpy as np


class Vec3d:
    def __init__(self, x: float, y: float, z: float):
        self.x = x
        self.y = y
        self.z = z


class Triangle:
    def __init__(self, points: list[Vec3d]):
        if len(points) != 3:
            raise Exception('points has wrong length of ' + len(points).__str__())
        self.points = points


class Mesh:
    Triangle("a")
    def __init__(self, triangles: list[Triangle]):
        self.triangles = triangles


def create_cube() -> Mesh:
    south1 = Triangle([Vec3d(0, 0, 0), Vec3d(0, 1, 0), Vec3d(1, 1, 0)])
    south2 = Triangle([Vec3d(0, 0, 0), Vec3d(1, 1, 0), Vec3d(1, 0, 0)])

    east1 = Triangle([Vec3d(1, 0, 0), Vec3d(1, 1, 0), Vec3d(1, 1, 1)])
    east2 = Triangle([Vec3d(1, 0, 0), Vec3d(1, 1, 1), Vec3d(1, 0, 1)])

    north1 = Triangle([Vec3d(1, 0, 1), Vec3d(1, 1, 1), Vec3d(0, 1, 1)])
    north2 = Triangle([Vec3d(1, 0, 1), Vec3d(0, 1, 1), Vec3d(0, 0, 1)])

    west1 = Triangle([Vec3d(0, 0, 1), Vec3d(0, 1, 1), Vec3d(0, 1, 0)])
    west2 = Triangle([Vec3d(0, 0, 1), Vec3d(0, 1, 0), Vec3d(0, 0, 0)])

    top1 = Triangle([Vec3d(0, 1, 0), Vec3d(0, 1, 1), Vec3d(1, 1, 1)])
    top2 = Triangle([Vec3d(0, 1, 0), Vec3d(1, 1, 1), Vec3d(1, 1, 0)])

    bottom1 = Triangle([Vec3d(1, 0, 1), Vec3d(0, 0, 1), Vec3d(0, 0, 0)])
    bottom2 = Triangle([Vec3d(1, 0, 1), Vec3d(0, 0, 0), Vec3d(1, 0, 0)])

    return Mesh([south1, south2, east1, east2, north1, north2, west1, west2, top1, top2, bottom1, bottom2])


SCREEN_HEIGHT = 640
SCREEN_WIDTH = 480
F_NEAR = 0.1
F_FAR = 1000.0
F_FOV = 90.0
F_ASPECT_RATIO = SCREEN_HEIGHT / SCREEN_WIDTH
F_FOV_RAD = 1.0 / np.tan(F_FOV * 0.5 / 180.0 * np.pi)
PROJECTION_MATRIX = np.array([[F_ASPECT_RATIO * F_FOV_RAD, 0, 0, 0],
                              [0, F_FOV_RAD, 0, 0],
                              [0, 0, F_FAR / (F_FAR - F_NEAR), 1.0],
                              [0, 0, (-F_FAR * F_NEAR) / (F_FAR - F_NEAR), 0]])


def x_rotation_matrix(theta):
    return np.array([[1.0, 0, 0, 0],
                     [0, np.cos(theta*0.5), np.sin(theta*0.5), 0],
                     [0, -np.sin(theta*0.5), np.cos(theta*0.5), 0],
                     [0, 0, 0, 1.0]])


def z_rotation_matrix(theta):
    return np.array([[np.cos(theta), np.sin(theta), 0, 0],
                     [-np.sin(theta), np.cos(theta), 0, 0],
                     [0, 0, 1.0, 0],
                     [0, 0, 0, 1.0]])


# Add the w component before the multiplication
def matrix_multiplication(point, matrix) -> Vec3d:
    array_point = np.array([point.x, point.y, point.z, 1])
    projected_point = np.matmul(array_point, matrix)
    if projected_point[3] != 0:
        projected_point = projected_point / projected_point[3]
    return Vec3d(projected_point[0], projected_point[1], projected_point[2])


def copy_triangle(to_be_copied: Triangle) -> Triangle:
    return Triangle([Vec3d(to_be_copied.points[0].x, to_be_copied.points[0].y, to_be_copied.points[0].z),
                     Vec3d(to_be_copied.points[1].x, to_be_copied.points[1].y, to_be_copied.points[1].z),
                     Vec3d(to_be_copied.points[2].x, to_be_copied.points[2].y, to_be_copied.points[2].z)])


def main():
    # Initialize some stuff
    pygame.init()
    pygame.display.set_caption("3d Renderer Engine")
    window = pygame.display.set_mode((SCREEN_HEIGHT, SCREEN_WIDTH))
    running = True
    old_time = pygame.time.get_ticks()
    rot_theta = 0

    # Create a cube mesh
    cubeMesh = create_cube()

    # Basic pygame loop
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        # Some stuff for the rotation
        new_time = pygame.time.get_ticks()
        elapsed_time = new_time - old_time
        rot_theta += 0.001 * elapsed_time

        # Clear screen on each new frame
        window.fill(pygame.Color("black"))
        count = 0

        # Project each triangle in the mesh
        for tri in cubeMesh.triangles:
            count += 1
            # if not(5 <= count < 7):
            #     continue

            # Apply z rotation
            triz1 = matrix_multiplication(tri.points[0], z_rotation_matrix(rot_theta))
            triz2 = matrix_multiplication(tri.points[1], z_rotation_matrix(rot_theta))
            triz3 = matrix_multiplication(tri.points[2], z_rotation_matrix(rot_theta))

            # Apply x rotation
            trizx1 = matrix_multiplication(triz1, x_rotation_matrix(rot_theta))
            trizx2 = matrix_multiplication(triz2, x_rotation_matrix(rot_theta))
            trizx3 = matrix_multiplication(triz3, x_rotation_matrix(rot_theta))

            # Translate triangle into z direction so it appears smaller and fits into the screen
            translated_triangle = Triangle([trizx1, trizx2, trizx3])
            translated_triangle.points[0].z += 3.0
            translated_triangle.points[1].z += 3.0
            translated_triangle.points[2].z += 3.0

            # Project the matrix to 2D
            tri1 = matrix_multiplication(translated_triangle.points[0], PROJECTION_MATRIX)
            tri2 = matrix_multiplication(translated_triangle.points[1], PROJECTION_MATRIX)
            tri3 = matrix_multiplication(translated_triangle.points[2], PROJECTION_MATRIX)

            # Scale triangles into view
            tri1.x += 1.0
            tri1.y += 1.0
            tri2.x += 1.0
            tri2.y += 1.0
            tri3.x += 1.0
            tri3.y += 1.0

            tri1.x *= 0.5 * SCREEN_WIDTH
            tri1.y *= 0.5 * SCREEN_HEIGHT
            tri2.x *= 0.5 * SCREEN_WIDTH
            tri2.y *= 0.5 * SCREEN_HEIGHT
            tri3.x *= 0.5 * SCREEN_WIDTH
            tri3.y *= 0.5 * SCREEN_HEIGHT

            # Draw wireframe
            pygame.draw.polygon(surface=window, color=(0, 255, 0), points=[(tri1.x, tri1.y), (tri2.x, tri2.y), (tri3.x, tri3.y)], width=1)
        pygame.display.update()
        old_time = new_time


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
